package com.jkantech.cpm.model

data class Equipment (
    val id:Long,
    val name:String,
    val description:String,
    val status:String,
    val isActive:Boolean,
    val categoryName:String)