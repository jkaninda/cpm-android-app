package com.jkantech.cpm.model

data class Dashboard(
    val success:Boolean=false,
    val message:String,
    val equipementCount:Int=0,
    val equipementEnPanne:Int=0,
    val equipementEnMarche:Int=0,
)
