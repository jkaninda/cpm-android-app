package com.jkantech.cpm.model

data class UpdateData(
    val success:Boolean,
    val message:String,
    val title:String,
    val version:String,
    val versionCode:Int,
    val filename:String,
    val link:String,
    val isImportant:Boolean
)
