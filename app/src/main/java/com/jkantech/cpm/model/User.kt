package com.jkantech.cpm.model

data class User(
    val id:Long,
    val name:String,
    val email:String,
    val phone:String,
    val address:String,
    val lang:String,
    val avatar:String,
    val role_id:Int,
    val role:String,
    val status:String,
    val last_login:String,
    val created_at:String,
)
