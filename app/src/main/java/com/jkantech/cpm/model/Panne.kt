package com.jkantech.cpm.model

import android.util.Log

data class Panne(
    val id:Long,
    val title:String,
    val description:String,
    val delay:Int,
    val state:String,
    val equipement_id:Long,
    val equipementName:String,
    val remainDelay:RemainDelay

            )

data class RemainDelay (
    val status:Boolean,
    val remain:Int,
    val message:String
    )
