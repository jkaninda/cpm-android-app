package com.jkantech.cpm

import android.Manifest
import android.app.DownloadManager
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.*
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.lifecycle.coroutineScope
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.jkantech.cpm.databinding.ActivityMainBinding
import com.jkantech.cpm.model.UpdateData
import com.jkantech.cpm.retrofiit.APIService
import com.jkantech.cpm.utils.Utils
import com.jkantech.napataadmin.Librairies.CustomeDataStore
import com.jkantech.napataadmin.Librairies.FileStore
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var cpmApp:CpmApp
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        FileStore.init(this)

        val navView: BottomNavigationView = binding.navView

        val navController = findNavController(R.id.nav_host_fragment_activity_main)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,R.id.navigation_equipment,R.id.navigation_messages, R.id.navigation_notifications
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)


        if (!checkPermission()) {
            requestPermission()
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
      menuInflater.inflate(R.menu.main_menu,menu)
        return super.onCreateOptionsMenu(menu)

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.about->{
                Utils.showToast(this,"About");
            }
        }
        return super.onOptionsItemSelected(item)
    }


    private fun checkUpdate() {
        val parameters: HashMap<String, String> = HashMap()
        val vCode = BuildConfig.VERSION_CODE
        parameters["vCode"] = vCode.toString()

        APIService().checkUpdate(parameters).enqueue(object : Callback<UpdateData> {
            override fun onResponse(call: Call<UpdateData>, response: Response<UpdateData>) {
                if (response.isSuccessful) {
                    response.body().let {
                        if (it != null) {
                            if (it.success) {
                                Log.i("CheckUpdate", "Update availbale")

                                Handler(Looper.getMainLooper()).postDelayed({
                                    updateDialog(it.title,it.message,it.filename,it.link,it.isImportant)
                                }, 1500)

                            } else {
                                Log.i("CheckUpdate", "No update available")

                            }

                        } else {
                            Log.i("CheckUpdate", "No update available")


                        }
                    }

                }
            }

            override fun onFailure(call: Call<UpdateData>, t: Throwable) {
                Log.i("CheckUpdate", "Error no internet")
            }
        })
    }

        private fun updateDialog(title:String,message:String,fileName: String,link:String,isImportant:Boolean=true){
            val alertDialog=  AlertDialog.Builder(this)
            alertDialog.apply {
                setTitle(title)
                setMessage(message)
                setIcon(R.drawable.ic_baseline_update_black)
                setPositiveButton(getString(R.string.valid_update), DialogInterface.OnClickListener { dialogInterface, i ->
//                    startActivity(
//                        Intent(
//                            Intent.ACTION_VIEW,
//                            Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
//                        )
//                    )
//                    dialogInterface.dismiss()
                    downloadFile(fileName,"",link)


                })
                if (isImportant){
                    Log.d("Update","Is important")
                }else{
                    setNegativeButton("Fermer"
                    ) { dialogInterface: DialogInterface, _: Int ->

                        dialogInterface.dismiss()


                    }

                }


                setCancelable(false)
                create()
                show()
            }
        }

    private fun downloadFile(fileName : String, desc :String, url : String){
        val folderName=getString(R.string.app_name)
        val path = Environment.getExternalStorageDirectory()
        val file= File("$path/Download/$folderName",fileName)
        if (file.exists()){
            Utils.showToast(this,getString(R.string.file_exist)+", veuillez l'installer SVP !!")
            return
        }

        // fileName -> fileName with extension

        val request = DownloadManager.Request(Uri.parse(url))
            .setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
            .setTitle(fileName)
            .setDescription(desc)
            .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
            .setAllowedOverMetered(true)
            .setAllowedOverRoaming(false)
            .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, File.separator + getString(R.string.app_name) + File.separator + fileName)

        val downloadManager= getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadID = downloadManager.enqueue(request)
        Utils.showToast(this,"Veuillez vérifier la progression dans la barre de notification, téléchargement encours...")

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this@MainActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION) ===
                                PackageManager.PERMISSION_GRANTED)) {
                        //Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                        Log.d("Permission","Permission Granted")
                    }
                } else {
                    Toast.makeText(this, "Permission refusée", Toast.LENGTH_SHORT).show()
                }
                return
            }
            2->{

                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this@MainActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION) ===
                                PackageManager.PERMISSION_GRANTED)) {
                        //Toast.makeText(this, "Permission Granted", Toast.LENGTH_SHORT).show()
                        Log.d("Permission","Permission Granted")

                    }
                } else {
                    Toast.makeText(this, "Permission refusée", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }


    }


    private fun checkPermission():Boolean{

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            Environment.isExternalStorageManager()
        }else{
            val readCheck= ContextCompat.checkSelfPermission(applicationContext,
                Manifest.permission.READ_EXTERNAL_STORAGE
            )
            val writeCheck= ContextCompat.checkSelfPermission(applicationContext,
                Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            readCheck== PackageManager.PERMISSION_GRANTED && writeCheck == PackageManager.PERMISSION_GRANTED
        }

    }
    private fun requestPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {

            try {
                val intent = Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                intent.addCategory("android.intent.category.DEFAULT")
                intent.data = Uri.parse(String.format("package:%s", applicationContext.packageName))
                startActivityForResult(intent, PERMISSION_REQUEST_CODE)
            } catch (e: Exception) {
                val intent = Intent()
                intent.action = Settings.ACTION_MANAGE_ALL_FILES_ACCESS_PERMISSION
                startActivityForResult(intent, PERMISSION_REQUEST_CODE)
            }
            Log.d("Permission", "Is External Manager permission")

        } else {

            when {
                ContextCompat.checkSelfPermission(
                    this@MainActivity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) !== PackageManager.PERMISSION_GRANTED -> {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(
                            this@MainActivity,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        )
                    ) {
                        ActivityCompat.requestPermissions(
                            this@MainActivity,
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            PERMISSION_REQUEST_CODE
                        )
                    } else {
                        ActivityCompat.requestPermissions(
                            this@MainActivity,
                            arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                            PERMISSION_REQUEST_CODE
                        )
                    }

                }

            }
        }
    }






    override fun onResume() {
        lifecycle.coroutineScope.launch {
            checkUpdate()
        }
        super.onResume()
    }
    companion object{
        const val PERMISSION_REQUEST_CODE=2296

    }

}