package com.jkantech.cpm.ui.equipments

import android.content.Intent
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.jkantech.cpm.R
import com.jkantech.cpm.adapter.EquipmentAdapter
import com.jkantech.cpm.data.ResponseData
import com.jkantech.cpm.databinding.FragmentMachineBinding
import com.jkantech.cpm.librairies.Serializer
import com.jkantech.cpm.model.Equipment
import com.jkantech.cpm.retrofiit.APIService
import com.jkantech.cpm.ui.activities.EquipmentDetailActivity
import com.jkantech.cpm.utils.Constants
import com.jkantech.cpm.utils.Utils
import com.jkantech.napataadmin.Librairies.FileStore
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class EquipmentFragment : Fragment(),View.OnClickListener {
    private lateinit var recyclerView: RecyclerView
    private lateinit var equipmentAdapter:EquipmentAdapter
    private lateinit var equipments:MutableList<Equipment>
    private var _binding: FragmentMachineBinding?=null

    private val binding get() =_binding!!

    companion object {
        fun newInstance() = EquipmentFragment()
    }

    private lateinit var viewModel: EquipmentViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentMachineBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(EquipmentViewModel::class.java)
        recyclerView=binding.include.machineRecyclerView

        binding.swipe.setOnRefreshListener {
            initData()
        }
        binding.swipe.isRefreshing=true

        loadCache()

        initView()
        return binding.root

    }

    private fun initView(){

        viewModel.machinesLiveData.observe(viewLifecycleOwner, {
            equipmentAdapter = EquipmentAdapter(requireContext(), it,this)
            recyclerView.adapter = equipmentAdapter
            recyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            recyclerView.setHasFixedSize(true)
            equipments=it
        })
        initData()

    }

    private fun initData(){

        APIService.invoke().getEquipments().enqueue(object :Callback<ResponseData<Equipment>>{
            override fun onResponse(
                call: Call<ResponseData<Equipment>>,
                response: Response<ResponseData<Equipment>>
            ) {
                response.body().let {
                    if (it != null) {
                        if (it.success){
                            viewModel.machinesLiveData.value=it.data
                            FileStore.saveFIle(Gson().toJson(it.data), Constants.EQUIPMENTDATA)


                        }



                    }
                }
                binding.swipe.isRefreshing=false
            }

            override fun onFailure(call: Call<ResponseData<Equipment>>, t: Throwable) {
                Utils.showToast(requireContext(), getString(R.string.server_not_found))
                binding.swipe.isRefreshing=false
            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(EquipmentViewModel::class.java)
    }

    override fun onClick(v: View?) {
        if (v!=null){
            when(v.id){
                R.id.machine_cardView->{
                    val equipment=equipments[v.tag as Int]
                    val intent=Intent(requireContext(),EquipmentDetailActivity::class.java)
                    intent.putExtra("id",equipment.id)
                    intent.putExtra("name",equipment.name)
                    startActivity(intent)

                }
            }
        }
    }

    private fun loadCache(){
        if (FileStore.checkFile(Constants.EQUIPMENTDATA)) {
            viewModel.machinesLiveData.value=Serializer().fromJson(FileStore.readFile(Constants.EQUIPMENTDATA),
                Serializer.Equipments::class.java)
            Log.d("Dashboard","File exist ")
        }else{
            Log.d("Dashboard","File doesn't exist ")

        }
    }

}