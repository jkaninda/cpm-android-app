package com.jkantech.cpm.ui.equipments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jkantech.cpm.R
import com.jkantech.cpm.utils.Utils


class EquipmentInfoFragment(private val equipmentId: Long) : Fragment() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val root= inflater.inflate(R.layout.fragment_equipment_info, container, false)

        Utils.showToast(requireContext(),equipmentId.toString())
        return root;
    }

    companion object {


    }
}