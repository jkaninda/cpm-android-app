package com.jkantech.cpm.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContentProviderCompat.requireContext
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.viewpager.widget.ViewPager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.tabs.TabLayout
import com.jkantech.cpm.R
import com.jkantech.cpm.adapter.MyAdapter
import com.jkantech.cpm.databinding.ActivityEquipmentDetailBinding
import com.jkantech.cpm.utils.LoadingDialog

class EquipmentDetailActivity : AppCompatActivity() {
    private var _binding:ActivityEquipmentDetailBinding? =null
    private val binding get() = _binding!!
    lateinit var fab: FloatingActionButton
    lateinit var loadingDialog: LoadingDialog

    lateinit var tabLayout: TabLayout
    lateinit var viewPager: ViewPager
    private var id:Long=0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding= ActivityEquipmentDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        val intent=intent
        id=intent.getLongExtra("id",0)
        val name=intent.getStringExtra("name")
        title=name

        tabLayout =binding.tabLayout
        viewPager = binding.viewPager


        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = MyAdapter(this,supportFragmentManager,id)
        viewPager.adapter = adapter

        tabLayout.setupWithViewPager(viewPager)
        viewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })


    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }
}