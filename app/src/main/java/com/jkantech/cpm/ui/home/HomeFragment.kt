package com.jkantech.cpm.ui.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.jkantech.cpm.R
import com.jkantech.cpm.databinding.FragmentHomeBinding
import com.jkantech.cpm.librairies.Serializer
import com.jkantech.cpm.model.Dashboard
import com.jkantech.cpm.retrofiit.APIService
import com.jkantech.cpm.utils.Constants
import com.jkantech.napataadmin.Librairies.FileStore
import retrofit2.Call
import retrofit2.Response

class HomeFragment : Fragment(),View.OnClickListener {

    private lateinit var homeViewModel: HomeViewModel
    private var _binding: FragmentHomeBinding? = null


    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root
        binding.include.cardMachine.setOnClickListener(this)
        binding.include.cardPanne.setOnClickListener(this)
        binding.include.cardRapport.setOnClickListener(this)

        binding.homeSwipe.isRefreshing=true

        homeViewModel.dashboardLiveData.observe(viewLifecycleOwner,{
            binding.include.tvAllMachines.text = it.equipementCount.toString()
            binding.include.txtAllWorking.text = it.equipementEnMarche.toString()
            binding.include.txtAllDamaged.text = it.equipementEnPanne.toString()
        })


        binding.homeSwipe.setOnRefreshListener {
            getData()
        }

        getData()

        loadCache()



        return root
    }
    private fun getData(){


        APIService.invoke().dashboard().enqueue(object :retrofit2.Callback<Dashboard>{
            override fun onResponse(call: Call<Dashboard>, response: Response<Dashboard>) {

                response.body().let {

                    if (it != null) {
                        Toast.makeText(requireContext(), it.message, Toast.LENGTH_SHORT).show()
                        homeViewModel.dashboardLiveData.value=it
                        FileStore.saveFIle(Gson().toJson(it,Dashboard::class.java),Constants.DASHBORDDATACACHE)


                    }
                }
                binding.homeSwipe.isRefreshing=false

            }

            override fun onFailure(call: Call<Dashboard>, t: Throwable) {
                Toast.makeText(requireContext(), "Error", Toast.LENGTH_SHORT).show()
                binding.homeSwipe.isRefreshing=false

            }

        })
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onClick(v: View?) {
        if (v!=null){
            when(v.id){
                R.id.card_machine->{
                    findNavController().navigate(R.id.navigation_equipment)
                }
                R.id.card_panne->{
                    findNavController().navigate(R.id.navigation_pannes)
                }
                R.id.card_rapport->{
                    findNavController().navigate(R.id.navigation_machine_detail_activity)
                }
            }
        }
    }

    private fun loadCache(){
        if (FileStore.checkFile(Constants.DASHBORDDATACACHE)) {
            homeViewModel.dashboardLiveData.value=Serializer().fromJson(FileStore.readFile(Constants.DASHBORDDATACACHE),Dashboard::class.java)
            Log.d("Dashboard","File exist ")
        }else{
            Log.d("Dashboard","File doesn't exist ")

        }
    }
}