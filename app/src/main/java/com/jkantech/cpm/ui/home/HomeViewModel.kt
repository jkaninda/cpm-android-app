package com.jkantech.cpm.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jkantech.cpm.model.Dashboard

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val text: LiveData<String> = _text

    val dashboardLiveData:MutableLiveData<Dashboard> by lazy {
        MutableLiveData<Dashboard>()
    }
}