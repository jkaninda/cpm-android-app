package com.jkantech.cpm.ui.equipments

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jkantech.cpm.model.Equipment

class EquipmentViewModel : ViewModel() {


    val machinesLiveData: MutableLiveData<MutableList<Equipment>> by lazy {
        MutableLiveData<MutableList<Equipment>>()

    }
}