package com.jkantech.cpm.ui.pannes

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.jkantech.cpm.R
import com.jkantech.cpm.adapter.EquipmentAdapter
import com.jkantech.cpm.adapter.PanneAdapter
import com.jkantech.cpm.data.ResponseData
import com.jkantech.cpm.databinding.PanneFragmentBinding
import com.jkantech.cpm.librairies.Serializer
import com.jkantech.cpm.model.Panne
import com.jkantech.cpm.retrofiit.APIService
import com.jkantech.cpm.ui.equipments.EquipmentViewModel
import com.jkantech.cpm.utils.Constants
import com.jkantech.cpm.utils.Constants.Companion.PANNESDATA
import com.jkantech.cpm.utils.Utils
import com.jkantech.napataadmin.Librairies.FileStore
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PanneFragment : Fragment(),View.OnClickListener {
    private lateinit var recyclerView: RecyclerView
    private lateinit var pannesAdapter: PanneAdapter
    private lateinit var pannes:MutableList<Panne>
    private var _binding: PanneFragmentBinding?=null

    private val binding get() =_binding!!
    companion object {
        fun newInstance() = PanneFragment()
    }

    private lateinit var viewModel: PanneViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = PanneFragmentBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this).get(
            PanneViewModel::class.java)
        recyclerView=binding.include.recyclerView

        binding.swipe.setOnRefreshListener {
            getData()
        }
        binding.swipe.isRefreshing=true

        viewModel.equipmentLiveData.observe(viewLifecycleOwner, {
            pannesAdapter = PanneAdapter(requireContext(), it,this)
            recyclerView.adapter = pannesAdapter
            recyclerView.layoutManager = LinearLayoutManager(requireActivity(), LinearLayoutManager.VERTICAL, false)
            recyclerView.setHasFixedSize(true)
            pannes=it
        })

        loadCache()
        getData()

        return binding.root
    }
    private fun getData(){


        APIService.invoke().getPannes().enqueue(object :Callback<ResponseData<Panne>>{
            override fun onResponse(
                call: Call<ResponseData<Panne>>,
                response: Response<ResponseData<Panne>>
            ) {

                response.body().let {
                    if (it != null) {
                        viewModel.equipmentLiveData.value=it.data
                        FileStore.saveFIle(
                            Gson().toJson(it.data),PANNESDATA)
                    }

                }
                binding.swipe.isRefreshing=false

            }

            override fun onFailure(call: Call<ResponseData<Panne>>, t: Throwable) {
                Utils.showToast(requireContext(), getString(R.string.server_not_found))
                binding.swipe.isRefreshing=false

            }

        })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(PanneViewModel::class.java)
    }

    override fun onClick(v: View?) {
    }
    private fun loadCache(){
        if (FileStore.checkFile(PANNESDATA)) {
            viewModel.equipmentLiveData.value= Serializer().fromJson(FileStore.readFile(PANNESDATA), Serializer.Pannes::class.java)
            Log.d("Pannes","File exist ")
        }else{
            Log.d("Pannes","File doesn't exist ")

        }
    }
}