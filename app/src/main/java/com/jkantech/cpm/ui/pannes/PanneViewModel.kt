package com.jkantech.cpm.ui.pannes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jkantech.cpm.model.Panne

class PanneViewModel : ViewModel() {

    val equipmentLiveData:MutableLiveData<MutableList<Panne>> by lazy {
        MutableLiveData<MutableList<Panne>>()
    }
}