package com.jkantech.cpm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.jkantech.cpm.R
import com.jkantech.cpm.model.Equipment
import com.jkantech.cpm.model.Panne

class PanneAdapter(context: Context, private val pannes:MutableList<Panne>, val onClickListener: View.OnClickListener):RecyclerView.Adapter<PanneAdapter.ViewHolder>() {
   inner class ViewHolder (view:View):RecyclerView.ViewHolder(view){
        val machine_cardView: CardView =itemView.findViewById(R.id.machine_cardView)
        val title: TextView =machine_cardView.findViewById(R.id.title)
        val model: TextView =machine_cardView.findViewById(R.id.tv_model)
        val tvState: TextView =machine_cardView.findViewById(R.id.tv_state)
        val description: TextView =machine_cardView.findViewById(R.id.tv_description)
        val info: ImageView =machine_cardView.findViewById(R.id.info)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val panne=pannes[position]
        holder.machine_cardView.tag=position
        holder.machine_cardView.setOnClickListener(onClickListener)
        holder.title.text=panne.title
        holder.tvState.text=panne.state
        holder.model.text=panne.equipementName
        holder.description.text=panne.description

        when(panne.state){
            "pending"-> holder.info.setImageResource(R.drawable.ic_baseline_pending_24)
            "processing"-> holder.info.setImageResource(R.drawable.ic_baseline_change_circle_24)
            "done"-> holder.info.setImageResource(R.drawable.ic_baseline_check_circle_outline_green)
            else->holder.info.setImageResource(R.drawable.ic_baseline_error_outline_red)
        }

    }

    override fun getItemCount(): Int {
        return pannes.size
    }
}