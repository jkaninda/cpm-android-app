package com.jkantech.cpm.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.jkantech.cpm.R
import com.jkantech.cpm.model.Equipment

class EquipmentAdapter(context: Context, private val equipments:MutableList<Equipment>, val onClickListener: View.OnClickListener):RecyclerView.Adapter<EquipmentAdapter.ViewHolder>() {
   inner class ViewHolder (view:View):RecyclerView.ViewHolder(view){
        val machine_cardView: CardView =itemView.findViewById(R.id.machine_cardView)
        val title: TextView =machine_cardView.findViewById(R.id.title)
        val model: TextView =machine_cardView.findViewById(R.id.tv_model)
        val tvState: TextView =machine_cardView.findViewById(R.id.tv_state)
        val description: TextView =machine_cardView.findViewById(R.id.tv_description)
        val info: ImageView =machine_cardView.findViewById(R.id.info)

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_equipment,parent,false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val equipment1=equipments[position]
        holder.machine_cardView.tag=position
        holder.machine_cardView.setOnClickListener(onClickListener)
        holder.title.text=equipment1.name
        holder.tvState.text=equipment1.status
        holder.model.text=equipment1.categoryName
        holder.description.text=equipment1.description
        if (equipment1.isActive){
            holder.info.setImageResource(R.drawable.ic_baseline_radio_button_checked_24)
        }else{
            holder.info.setImageResource(R.drawable.ic_baseline_stop_circle_24)
        }

    }

    override fun getItemCount(): Int {
        return equipments.size
    }
}