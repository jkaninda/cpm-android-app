package com.jkantech.cpm.adapter

import android.content.Context
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.jkantech.cpm.ui.equipments.EquipmentAnalysisFragment
import com.jkantech.cpm.ui.equipments.EquipmentInfoFragment
import com.jkantech.cpm.ui.equipments.EquipmentLogFragment

class MyAdapter(private val myContext: Context, fm: FragmentManager,val equipmentIdp:Long) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> {
                EquipmentInfoFragment(equipmentIdp)
            }
            1 -> {
                EquipmentLogFragment(equipmentIdp)
            }
            2 -> {
                EquipmentAnalysisFragment(equipmentIdp)
            }
            else -> EquipmentInfoFragment(equipmentIdp)

        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> {
               "Equipment Info"
            }
            1 -> {
                "Equipment Log"

            }
            2 -> {
               "Equipment Analyse"
            }
            else -> "Equipment Info"
        }



        return super.getPageTitle(position)
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        super.destroyItem(container, position, `object`)
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return 3//totalTabs
    }
}
