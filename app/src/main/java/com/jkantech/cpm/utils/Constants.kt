package com.jkantech.cpm.utils

class Constants {
    companion object{
        var isOnline=true
        private fun baseUrl():String{
            return if (isOnline){
                "https://cpm.jkantech.com/"
            }else{
                "http://192.168.0.100/"

            }
        }
        val baseURL=baseUrl()
        val baseURLAPI= baseURL+"api/"
        const val DASHBORDDATACACHE="107928988_50352.temp"
        const val EQUIPMENTDATA="107928988_50353.temp"
        const val PANNESDATA="107928988_50354.temp"


        const val USERSDDATACACHE="107928988_50353.temp"
        const val USERDDATACACHE="107928988_50354.temp"
    }
}