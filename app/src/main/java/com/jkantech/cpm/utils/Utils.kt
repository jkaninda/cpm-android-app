package com.jkantech.cpm.utils

import android.content.Context
import android.widget.Toast
import com.google.gson.Gson

object Utils {
    fun showToast(context: Context,message:String?) {
        return Toast.makeText(context, message.orEmpty(), Toast.LENGTH_LONG).show()
    }

}