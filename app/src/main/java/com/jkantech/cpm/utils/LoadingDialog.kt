package com.jkantech.cpm.utils

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import com.jkantech.cpm.R

class LoadingDialog(private var activity: Activity) {
    var dialog: Dialog? = null


    fun startLoadingDialog(cancelable:Boolean=false) {
        dialog = Dialog(activity)
        dialog?.apply {
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setContentView(R.layout.loading_dialog)
            setCancelable(cancelable)
            show()
        }
    }

        fun dismissDialog() {
            dialog?.dismiss()
        }


}