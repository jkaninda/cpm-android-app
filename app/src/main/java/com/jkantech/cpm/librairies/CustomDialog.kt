package com.jkantech.napataadmin.Librairies

import android.annotation.SuppressLint
import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.jkantech.cpm.R
import com.tapadoo.alerter.Alerter


class CustomDialog (private val activity: Activity){
    fun show(title:String,message:String,duration:Long=3000){
        AlertDialog.Builder(activity)
        Alerter.create(activity)
                .setTitle(title)
                .setText(message)
                .enableProgress(false)
                .setDuration(duration)
                .enableVibration(false)
                .enableSwipeToDismiss()
                .setBackgroundColorRes(R.color.red)
                .show()

    }

}