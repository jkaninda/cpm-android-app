package com.jkantech.napataadmin.Librairies

import android.content.Context

import java.io.File

object FileStore {
    @Suppress("UNREACHABLE_CODE")
    private lateinit var appContext: Context


    fun init(context: Context) {
        if(!FileStore::appContext.isInitialized) {
            appContext = context.applicationContext
        }
    }






         fun saveFIle(data: String?,filename:String){
            val baseFolder: File? = appContext.cacheDir
            // val baseFolder = FileManager.getCaheDir(appContext)

             //FileManager.getCaheDir(appContext)
            if (!baseFolder!!.exists()) baseFolder.mkdirs()
            val dataFile = File(baseFolder, filename)
            //File(dataFile).writeText(response)
            if (data != null) {
                dataFile.writeText(data)
            }

        }

         fun readFile(filename: String):String{
            val baseFolder: File? = appContext.cacheDir
            //val baseFolder = FileManager.getCaheDir(appContext)
            if (!baseFolder!!.exists()) baseFolder.mkdirs()
            val dataFile = File(baseFolder, filename)
            //val dataFile = File(baseFolder, filename)


             return dataFile.readText()

        }

    fun deleteFile(filename: String): Boolean {
        val baseFolder: File? = appContext.cacheDir
        if (!baseFolder!!.exists()) baseFolder.mkdirs()
        val dataFile = File(baseFolder,filename)
        return dataFile.delete()

    }
    fun clearCache():Boolean {
        val baseFolder: File? = appContext.cacheDir
        if (!baseFolder!!.exists()) baseFolder.mkdirs()
        return deleteDir(baseFolder)

    }

    fun checkFile(filename: String):Boolean{
        val baseFolder: File? = appContext.cacheDir
        //val baseFolder = FileManager.getCaheDir(appContext)
        if (!baseFolder!!.exists()) baseFolder.mkdirs()
        val dataFile = File(baseFolder,filename)
        return dataFile.exists()
    }


    private fun deleteDir(dir: File?): Boolean {
        if (dir != null && dir.isDirectory) {
            val children = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
        }
        return dir!!.delete()
    }


}


