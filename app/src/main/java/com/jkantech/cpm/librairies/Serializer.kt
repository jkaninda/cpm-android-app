package com.jkantech.cpm.librairies

import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import com.google.gson.Gson
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonSyntaxException
import com.google.gson.internal.Primitives
import com.jkantech.cpm.model.Dashboard
import com.jkantech.cpm.model.Equipment
import com.jkantech.cpm.model.Panne
import com.jkantech.cpm.model.User
import java.lang.reflect.Type

class  Serializer {
    fun <T> fromJson(objectTString: String?, classOfT: Class<T>?): T {
        return Gson().fromJson(objectTString, classOfT)
    }
    fun <T> toJson(objectOfT: T): String? {
        return Gson().toJson(objectOfT)
    }

class Equipments:ArrayList<Equipment>()
class Users:ArrayList<User>()
class Pannes:ArrayList<Panne>()
}


