package com.jkantech.napataadmin.Librairies

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import androidx.core.content.edit


@SuppressLint("StaticFieldLeak")
object CustomeDataStore {

    private lateinit var context: Context
    private lateinit var prefs:SharedPreferences
    //private lateinit var dataStore: DataStore<Preferences>

    fun init(contextr: Context){
        context =contextr
    }

    fun save(key:String, value:String?){
        //dataStore = DataStore
            //createDataStore(name = "settings")
        if (value!=null){
            prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context)
            prefs.edit {
                putString(key,value)
            }

        }else{
            Log.d("DATA","null")
        }



    }
    fun saveBoolean(key:String, value:Boolean=false){
        prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context)
        prefs.edit {
            putBoolean(key,value)
        }

    }

    fun restoreBoolean(key: String): Boolean {
        prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getBoolean(key, false)
    }
    fun restore(key: String): String? {
        prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getString(key, "")
    }

    fun restoreInt(key: String): Int {
        prefs = androidx.preference.PreferenceManager.getDefaultSharedPreferences(context)
        return prefs.getInt(key, 0)
    }


}