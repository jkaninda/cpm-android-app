package com.jkantech.napataadmin.Librairies

import android.annotation.SuppressLint
import android.graphics.Color
import android.view.View
import com.google.android.material.snackbar.Snackbar

@SuppressLint("StaticFieldLeak")
object CustomSnakbar {
    private lateinit var viewr: View
    fun init(view: View){
        viewr =view
    }

    fun show(message: String){
      val snackbar =Snackbar.make(viewr,message, Snackbar.LENGTH_LONG)
        snackbar.setAction("Fermer", View.OnClickListener {
            snackbar.dismiss()
        })
        snackbar.setBackgroundTint(Color.parseColor("#39434F"))
        snackbar.show()
    }
}