package com.jkantech.napataadmin.Librairies

import android.annotation.SuppressLint
import android.content.Context
import android.widget.Toast

@SuppressLint("StaticFieldLeak")
object CustomToast {
    private lateinit var context: Context

    fun init(contextr: Context){
        context =contextr
    }
    fun show(message:String?){
        if (message!=null){
            Toast.makeText(context,message, Toast.LENGTH_LONG).show()
        }

    }
}