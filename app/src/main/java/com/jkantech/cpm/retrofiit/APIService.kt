package com.jkantech.cpm.retrofiit

import com.jkantech.cpm.data.LoginData
import com.jkantech.cpm.data.ResponseData
import com.jkantech.cpm.model.Dashboard
import com.jkantech.cpm.model.Equipment
import com.jkantech.cpm.model.Panne
import com.jkantech.cpm.model.UpdateData
import com.jkantech.cpm.utils.Constants.Companion.baseURLAPI
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import kotlin.collections.HashMap

interface APIService {

    //Login
    @POST("login")
    fun login(@QueryMap params: HashMap<String?, String?>): Call<LoginData>

    //DashBoard
    @GET("dashboard")
    fun dashboard():Call<Dashboard>


    //Equipement
    @GET("equipements")
    fun getEquipments():Call<ResponseData<Equipment>>

    @GET("equipements/{id}")
    fun getEquipment(@Header("Authorization") token:String?,@QueryMap params: HashMap<String?, String?>)

    //Pannes
    @GET("pannes")
    fun getPannes():Call<ResponseData<Panne>>

    //Home
    /*@GET("home")
    fun  home(@Header("Authorization") token: String,@QueryMap params: HashMap<String?, String?>):Call<ResponseDataDetail<HomeData>>
*/

    @POST("services/checkUpdate")
    fun checkUpdate(@QueryMap params: HashMap<String, String>): Call<UpdateData>

    companion object {
        operator fun invoke(): APIService {
            return Retrofit.Builder()
                .baseUrl(baseURLAPI)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(APIService::class.java)
        }
    }
}