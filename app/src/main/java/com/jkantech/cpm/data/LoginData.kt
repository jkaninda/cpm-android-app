package com.jkantech.cpm.data

import com.jkantech.cpm.model.User

data class LoginData(
    val success:Boolean,
    val message:String,
    val locale:String,
    val token:String,
    val user: User

)
