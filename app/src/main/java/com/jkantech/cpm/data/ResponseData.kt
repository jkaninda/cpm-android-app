package com.jkantech.cpm.data

    data class ResponseData<T> (
        val success:Boolean,
        val message:String,
        val data:MutableList<T> ,
    )

