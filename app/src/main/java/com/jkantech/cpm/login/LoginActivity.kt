package com.jkantech.cpm.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import androidx.appcompat.widget.AppCompatButton
import com.jkantech.cpm.MainActivity
import com.jkantech.cpm.R
import com.jkantech.cpm.data.LoginData
import com.jkantech.cpm.databinding.ActivityLoginBinding
import com.jkantech.cpm.retrofiit.APIService
import com.jkantech.cpm.utils.CustomDataStore
import com.jkantech.cpm.utils.Utils
import retrofit2.Call
import retrofit2.Response

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    private lateinit var password:EditText
    private lateinit var userName:EditText
    private lateinit var loginButton: AppCompatButton


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        password=binding.include.password
        userName=binding.include.username
        loginButton=binding.include.loginButton
        binding.root
        CustomDataStore.init(this)

        binding.cancelAction.setOnClickListener {
            finish()
        }

        loginButton.setOnClickListener {
            when{
                userName.text.toString() ==""->Utils.showToast(this,"Veuillez entre le nom d'utilisateur")
                password.text.toString() ==""->Utils.showToast(this,"Veuillez entre le mot de passe")
                else->login()
            }
        }
    }
    private fun login(){
        binding.include.progressBar.visibility=View.VISIBLE
        val data:HashMap<String?,String?> = HashMap();
        data["email"] =userName.text.toString()
        data["password"] =password.text.toString()

        Log.d("Daily",data.toString())
        APIService.invoke().login(data).enqueue(object :retrofit2.Callback<LoginData>{
            override fun onResponse(
                call: Call<LoginData>,
                response: Response<LoginData>
            ) {
                binding.include.progressBar.visibility=View.GONE

                if (response.isSuccessful) {
                    response.body().let {

                        if (it != null) {
                            if (it.success) {
                                    CustomDataStore.storeBoolean("isConnected",true)
                                    CustomDataStore.store("userId",it.user.id.toString())
                                    CustomDataStore.store("name",it.user.name)
                                    //CustomDataStore.store("role",it.user.role)
                                    CustomDataStore.store("roleId",it.user.role_id.toString())
                                    CustomDataStore.store("token",it.token)
                                    CustomDataStore.store("locale",it.user.lang)
                                    successLogin()


                            }else{
                                Utils.showToast(this@LoginActivity, it.message)

                            }
                        }

                    }


                    }else {

                        Utils.showToast(this@LoginActivity, getString(R.string.server_not_found))

                    }


            }


            override fun onFailure(call: Call<LoginData>, t: Throwable) {
                Log.d("NetworkError",t.message.toString())
                binding.include.progressBar.visibility=View.GONE
                Utils.showToast(this@LoginActivity,getString(R.string.server_not_found))
            }

        })




    }

    private fun successLogin(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}